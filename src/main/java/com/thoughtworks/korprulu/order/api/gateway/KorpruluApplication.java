package com.thoughtworks.korprulu.order.api.gateway;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.ComponentScan;

@EnableZuulProxy
@EnableFeignClients
@SpringCloudApplication
@EnableConfigurationProperties
@ComponentScan("com.thoughtworks.korprulu.order")
@EntityScan({"com.thoughtworks.korprulu.order.infrastructure.persistence.po"})
public class KorpruluApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(KorpruluApplication.class)
            .bannerMode(Banner.Mode.OFF)
            .run(args);
    }
}
