package com.thoughtworks.korprulu.order.api.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.consul.ConditionalOnConsulEnabled;
import org.springframework.cloud.consul.discovery.ConsulDiscoveryClientConfiguration;
import org.springframework.cloud.consul.discovery.ConsulDiscoveryProperties;
import org.springframework.cloud.consul.discovery.ConsulLifecycle;
import org.springframework.cloud.consul.discovery.HeartbeatProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

@Configuration
@ConditionalOnConsulEnabled
@ConditionalOnProperty(value = "spring.cloud.consul.discovery.enabled", matchIfMissing = true)
@Import(ConsulDiscoveryClientConfiguration.class)
public class DiscoveryConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiscoveryConfiguration.class);

    @Autowired
    private ConsulDiscoveryClientConfiguration configuration;

    @Autowired
    private ApplicationContext context;

    @Bean
    public ConsulLifecycle consulLifecycle(ConsulDiscoveryProperties discoveryProperties,
        HeartbeatProperties heartbeatProperties) {

        String suffix;
        try {
            suffix = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            suffix = UUID.randomUUID().toString();
            LOGGER.error("get hostname error, {}", e.getMessage());
        }
        String instanceId = context.getId() + ConsulLifecycle.SEPARATOR + suffix;
        LOGGER.info("register service id: {}", instanceId);
        discoveryProperties.setInstanceId(instanceId);
        return configuration.consulLifecycle(discoveryProperties, heartbeatProperties);
    }
}
