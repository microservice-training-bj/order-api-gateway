#! /usr/bin/env bash

PRIVATE_REPO=http://10.202.129.124:8081
INTEG_REPO_PULL=172.21.3.76:5000
INTEG_REPO_PUSH=172.21.3.76:5001

PROJECT_NAME=korprulu
SERVICE_NAME=api-gateway
IMAGE_NAME=${PRIVATE_REPO}/${PROJECT_NAME}/${SERVICE_NAME}
# $GO_REVISION from pipeline environment variable
GO_SHORT_REVISION=${GO_REVISION:0:8}
IMAGE_TAG_NAME=${IMAGE_NAME}:${GO_PIPELINE_COUNTER}-${GO_SHORT_REVISION}
IMAGE_LATEST_NAME=${IMAGE_NAME}:latest
ENV=$2

function test-project {
    ./gradlew clean test
    return_code=$?
    return $return_code
}

function build-image {
#    docker login -u admin -p admin123 ${PRIVATE_REPO}
    # build jar package
    ./gradlew clean -x test build
    # build image

    echo "*************Image name is [${IMAGE_TAG_NAME}]*************"

}



case $1 in
    test)
        test-project
        ;;
    build)
        build-image
        ;;
    deploy)
        deploy
        ;;
    *)
        exit -1
esac
